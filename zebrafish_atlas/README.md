# Larval zebrafish brain dataset

Example workflow for reanalysing of Larval zebrafish brain dataset [1] with GeNePy3D. The dataset consists of up to 2000 traced neurons (in SWC format) and 36x2 symmetric brain region outlines (in NRRD format) of Larval zebrafish. 

The notebook **process.ipynb** contains processing steps e.g. import data, check intersection between traced neurons and brain region outlines, compute lengths, branching points, tortuosities, local 3d scales [2], etc. The processed data were then saved to *output/* (CSV format) and were analysed in **analyse.ipynb**.

The Larval zebrafish dataset can be downloaded from https://fishatlas.neuro.mpg.de.

**References**

[1] Michael Kunst, Eva Laurell, Nouwar Mokayes, Anna Kramer, Fumi Kubo, António M. Fernandes, Dominique Förster, MarcoDal Maschio, and Herwig Baier. A Cellular-Resolution Atlas of the Larval Zebrafish Brain.Neuron, 103(1):21–38.e5, July 2019.ISSN 0896-6273. doi: 10.1016/j.neuron.2019.04.034.

[2] Minh Son Phan, Katherine Matho, Jean Livet, Emmanuel Beaurepaire, and Anatole Chessel.  A scale-space approach for 3D neuronal traces analysis. bioRxiv, page 2020.06.01.127035, June 2020. doi:  10.1101/2020.06.01.127035. Publisher: ColdSpring Harbor Laboratory Section: New Results.
